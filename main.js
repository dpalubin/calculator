const display = document.querySelector('.display');
const numbers = Array.from(document.querySelectorAll('.number'));
const invalidNumbers = ["Overflow!", "NaN", "-NaN", "Infinity", "-Infinity"];

//Used to reset the value in display when we start typing a new number after an operator
function checkAfterOperator(){
  if(afterOperator) {
    afterOperator = false;
    display.textContent = '0';
    display.style.fontFamily = 'segment7, monospace';
  }
  if(afterEqual) {
    afterEqual = false;
    first = '';
  }
  validateNumber();
}

//Checks if number on display is invalid and clears if true. Returns whether number was invalid.
function validateNumber(){
  if(invalidNumbers.includes(display.textContent) || invalidNumbers.includes(first)){
      clear();
      return false;
  }
  return true;
}

//Prevent excessively long or large numbers from being displayed
function limitDisplayPrecision() {
  if(display.textContent.length >= 10){
    let number = parseFloat(display.textContent).toPrecision(10) - 0;  //Subtract 0 to hide trailing zeros
    if(number >= 1e9 || number <= -1e9){
      number = number.toExponential();
      display.style.fontFamily = 'monospace';
    }
    display.textContent = number;
  }
  if(invalidNumbers.includes(display.textContent)){
    display.textContent = "Overflow!";
    display.style.fontFamily = 'monospace';
  }
}

//Handles number input from event listener
function enterNumber(e) {
  let newNumber = (e.type === "keydown") ? e.key : this.textContent;
  checkAfterOperator();
  const negative = (display.textContent[0] === '-') ? '-' : '';
  //Make sure that we overwrite '0' value without removing a possible negative sign or decimal
  display.textContent = (parseInt(display.textContent) === 0 && !display.textContent.includes('.'))
                      ? negative + newNumber
                      : display.textContent + newNumber;
  limitDisplayPrecision();
}

//Clears display contents, stored numbers, and operators
function clear() {
  display.textContent = '0';
  display.style.fontFamily = 'segment7, monospace';
  first = '';
  operator = '';
  afterOperator = false;
  afterEqual = false;
}

//Converts number to a percent
function percent() {
  if(!validateNumber()) return;
  display.textContent = parseFloat(display.textContent) / 100.0;
  limitDisplayPrecision();
  if(first !== '') first = parseFloat(display.textContent);
}

//Backspace function for user to edit the current number
function back() {
  checkAfterOperator();  //Could potentially clear the entire number. Is this the correct behavior here? What is the user's intent?
  
  //-7 should become -0. However 7 should become 0, and -0 should become 0.  
  //The only time the negative will erased is if the value is -0.
  if(display.textContent === '-0' || display.textContent.length === 1){
    display.textContent = '0';
  } else {
    display.textContent = display.textContent.slice(0,-1);
    if(display.textContent === '-') display.textContent = '-0';
  }
}

//Adds a decimal place to the current number
function dot() {
  checkAfterOperator();
  if(!display.textContent.includes('.')) display.textContent += '.';
  limitDisplayPrecision();
}

//Toggles a number to negative/positive
function negative() {
  if(!validateNumber()) return;
  const content = display.textContent;
  display.textContent = (content[0] === '-') ? content.slice(1) : '-' + content;
  limitDisplayPrecision();
  if(first !== '') first = parseFloat(display.textContent);
}

//Handles buttons that control the mathematical operator (+, -, /, *)
function operate(e) {
  let newOperator = (e.type === "keydown") ? e.key : this.textContent;
  if(!validateNumber()) return;
  if(afterOperator){   //Only time this should happen is if the user entered the wrong operator on their last key press, or afterEqual
    operator = newOperator;
    afterEqual = false;
    return;
  }
  if(first){
    //Perform a chained calculation here
    const second = parseFloat(display.textContent);
    first = ops[operator](first, second);
    display.textContent = first;
    limitDisplayPrecision();
  } else {
    first = parseFloat(display.textContent);
  }
  operator = newOperator;
  afterOperator = true;
}

//Handles the equal button to solve the mathematical expression
function equal(e) {
  if(first && operator && !afterOperator){
    const second = parseFloat(display.textContent);
    first = ops[operator](first, second);
    display.textContent = first;
    limitDisplayPrecision();
    operator = '';
    afterOperator = true;
    afterEqual = true;  //After pressing equal, if user starts to press a new number, we should clear the first number.
  }
}

//Direct keyboard input to the appropriate function
function keyHandler(e){
  if(parseInt(e.key) >= 0){
    enterNumber(e);
  } else{
    switch(e.key){
      case '/':
      case "+":
      case '-':
      case '*':
        operate(e);
        break;
      case 'Enter':
      case '=':
        equal();
        break;
      case '%':
        percent();
        break;
      case '.':
        dot();
        break;
      case 'Backspace':
        back();
        break;
    }
  }
  e.preventDefault(); //Prevent unwanted behavior when pressing '/' or 'Enter' keys
}


let first = '';
let operator = '';
let afterOperator = false;  //Set to true when we press the operators + - * /. Set to false immediately after we start typing a new number.
let afterEqual = false; //Set to true immediately after pressing equal
const ops = {
  '+' : (a, b) => a + b,
  '-' : (a, b) => a - b,
  '*' : (a, b) => a * b,
  '/' : (a, b) => a / b,
}

numbers.forEach((item) => item.addEventListener('click', enterNumber));
document.querySelector('.clear').addEventListener('click', clear);
document.querySelector('.back').addEventListener('click', back);
document.querySelector('.percent').addEventListener('click', percent);
document.querySelector('.dot').addEventListener('click', dot);
document.querySelector('.negative').addEventListener('click', negative);

document.querySelectorAll('.operation').forEach((item) => item.addEventListener('click', operate));
document.querySelector('.equal').addEventListener('click', equal);

window.addEventListener('keydown', keyHandler);