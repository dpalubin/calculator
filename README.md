# Calculator

This project is part of The Odin Project (TOP) curriculum. The project page can be found [here](https://gitlab.com/dpalubin/calculator).

## License

Code for this project is provided via the MIT License, Copyright 2023 David Palubin.
